package myprojects.automation.assignment4;


import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private Actions builder;

    //інфа про продукт
    private String productNam;
    private String productPric;
    private int productQt;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        builder = new Actions(driver);
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        // 1) вхід в адмін панель
        driver.get(Properties.getBaseAdminUrl());
        WebElement loginField = driver.findElement(By.id("email"));
        loginField.sendKeys(login);
        WebElement passField = driver.findElement(By.id("passwd"));
        passField.sendKeys(password);
        WebElement enter = driver.findElement(By.name("submitLogin"));
        enter.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.titleIs("Dashboard • prestashop-automation"));
        Assert.assertEquals(driver.getTitle(), "Dashboard • prestashop-automation", "Unexpected title");
        //  throw new UnsupportedOperationException();
    }

    public void navigateToGoodsSubmenu() {
        WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));
        WebElement tovary = driver.findElement(By.id("subtab-AdminProducts"));
        builder.moveToElement(catalog).moveToElement(tovary).click(tovary).build().perform();
    }

    public void addNewGood() {
        WebElement newGood = driver.findElement(By.xpath("//*[@id=\"page-header-desc-configuration-add\"]/span"));
        newGood.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.titleIs("товары • prestashop-automation"));
    }

    public void createProduct(ProductData newProduct) {
        // TODO implement product creation scenario
        WebElement productName = driver.findElement(By.id("form_step1_name_1"));
        WebElement productQty = driver.findElement(By.id("form_step1_qty_0_shortcut"));
        WebElement productPrice = driver.findElement(By.id("form_step1_price_ttc_shortcut"));

        productNam = newProduct.getName();
        productQt = newProduct.getQty();
        productPric = newProduct.getPrice();
        System.out.println(productNam);
        System.out.println(productQt);
        System.out.println(productPric);

        productName.sendKeys(newProduct.getName());
        productQty.sendKeys(String.valueOf(newProduct.getQty()));
        productPrice.sendKeys(newProduct.getPrice());
        //throw new UnsupportedOperationException();
    }

    public void activation() {
        WebElement activationBtn = driver.findElement(By.className("switch-input"));
        activationBtn.click();
        WebElement msg = driver.findElement(By.className("growl-close"));
        msg.click();
    }

    public void saveProduct() {
        WebElement saveBtn = driver.findElement(By.xpath("//*[@id=\"form\"]/div[4]/div[2]/div/button[1]"));
        saveBtn.click();
        WebElement msg = driver.findElement(By.className("growl-close"));
        msg.click();
    }

    public void navigateToMainPage() {
        driver.get(Properties.getBaseUrl());
    }

    public void checkProductIsDisplayed() {
        WebElement allProductsLink = driver.findElement(By.xpath("//*[@id=\"content\"]/section/a"));
        allProductsLink.click();
        for(int i = 0; i <100; i++) {
            if (driver.findElements(By.xpath("//a[contains(text(), '" + productNam + "')]")).size() > 0) {
                WebElement prodText =  driver.findElement(By.xpath("//a[contains(text(), '" + productNam + "')]"));
                Assert.assertEquals(prodText.getText(), productNam, "Product is not finded");
                break;
            } else {
                driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/nav/div[2]/ul/li[3]/a")).click();
            }
        }
    }

    public void openProduct() {
        WebElement prodText =  driver.findElement(By.xpath("//a[contains(text(), '" + productNam + "')]"));
        prodText.click();
        String prodName = driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div[2]/h1")).getText();
        String prodQtyy = driver.findElement(By.className("product-quantities")).getText();
        String prodPricee = driver.findElement(By.xpath("//span[@content]")).getText();

        Assert.assertEquals(prodName, productNam.toUpperCase(), "Product is not finded");
        StringBuffer sb = new StringBuffer("По наличию " + productQt + " Товары");
        Assert.assertEquals(prodQtyy, "По наличию "+ productQt +" Товары" , "Qty is not finded");
        Assert.assertEquals(prodPricee, productPric + " ₴", "Price is not finded");
    }
    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        // TODO implement generic method to wait until page content is loaded

        // wait.until(...);
        // ...
    }
}
