package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.ProductData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    @DataProvider(name = "authentication")
    public Object[][] createData1() {
        return new Object[][]{
                {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}
        };
    }

    @Test(dataProvider = "authentication")
    public void createNewProduct(String login, String password) {
        // TODO implement test for product creation
        actions.login(login, password);
        actions.navigateToGoodsSubmenu();
        actions.addNewGood();
        actions.createProduct(ProductData.generate());
        actions.activation();
        actions.saveProduct();
    }
    @Test
    public void checkProductVisibility(){
        actions.navigateToMainPage();
        actions.checkProductIsDisplayed();
        actions.openProduct();
    }

    // TODO implement logic to check product visibility on website
}

